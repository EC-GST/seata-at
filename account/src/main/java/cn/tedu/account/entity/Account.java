package cn.tedu.account.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private Long id;
    private Long userId;
    private BigDecimal total; // BigDecimal 可以精确表示数字
    private BigDecimal used;
    private BigDecimal residue;
    private BigDecimal frozen;
}
