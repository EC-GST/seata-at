package cn.tedu.order.mapper;

import cn.tedu.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface OrderMapper extends BaseMapper<OrderMapper> {
    void create(Order order);
//    int insert(Order order);   BaseMapper 自带 的新增方法

}
